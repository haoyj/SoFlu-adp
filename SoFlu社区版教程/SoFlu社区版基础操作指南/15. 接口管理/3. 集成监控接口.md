### 3. 集成监控接口

#### 3.1 设置接口的集成监控

平台提供对接口进行指标上报、监控上报以及字段监控采集的功能。用户可以根据需求进行相关设置，并通过第三方运维集成平台对监控采集到的数据进行分析，其中采集到的数据包含接口请求总次数、响应时间及请求参数等。本节主要介绍如何设置接口的集成监控。

##### 3.1.1 操作步骤

a）新增一个接口，配置接口的基本信息，点击更多设置展开接口的其他设置，如图3-1所示：

![3-1](https://www.feisuanyz.com/fsimage/zc-image/jkgl/jcjk_1.png)

##### 图3-1 配置基本信息

b）更多设置中包含业务标签、指标上报、监控上报等设置，如图3-2所示：

![3-2](https://www.feisuanyz.com/fsimage/zc-image/jkgl/jcjk_2.png)

##### 图3-2 更多设置

c）输入业务标签，打开指标上报及监控上报的按钮，如图3-3所示：

##### 业务标签是接口请求的标识，接口的业务标签相同表示接口请求相同，业务标签不同表示接口请求不同，最多可输入5个业务标签。

![3-3](https://www.feisuanyz.com/fsimage/zc-image/jkgl/jcjk_3.png)

##### 图3-3 输入业务标签

d）配置完接口的基本信息，点击下一步，配置接口的入口参数。

e）请求参数中包含监控采集字段，新增一个入口参数，打开监控采集的按钮即可实现对接口字段的监控采集，如图3-4所示：

![3-4](https://www.feisuanyz.com/fsimage/zc-image/jkgl/jcjk_4.png)

##### 图3-4 新增入口参数

f）配置完接口的入口参数，点击提交。

g）完成对接口的编辑，执行接口的测试用例或调用该接口时，传入参数字段即会产生相应的数据。

#### 3.2 查看监控采集的数据

本节主要介绍如何查看监控采集到的数据。

##### 3.2.1 操作步骤

a）在浏览器的地址栏输入 http://ip地址:9091/actuator/prometheus ，即可查看监控上报的数据，包含请求接口、总的请求次数、总的响应时间、最大的一次响应时间等，如图3-5所示：

##### ip地址为对项目进行部署的服务器ip地址，9091为默认端口号。

![3-5](https://www.feisuanyz.com/fsimage/zc-image/jkgl/jcjk_6.png)

##### 图3-5 查看监控数据

b）可通过` promentheus `、` grafana `等第三方监控平台，对监控采集到的接口数据进行提取和分析监控等。

#### 3.3 集成` influxdb `数据库监控接口

全自动开发平台集成监控系统支持集成` influxdb `数据库实现对接口数据的监控。` influxdb `是一个开源的时序数据库，使用GO语言开发，特别适合用于处理和分析资源监控数据这种时序相关的数据，而` influxdb `自带的各种特殊函数如求标准差、随机取样数据、统计数据变化比等，使数据统计和实时分析变得十分方便。本节将介绍如何配置` influxdb `数据库实现接口监控。

##### 注意：只有在安装本地客户端或者部署项目时才可以配置` influxdb `数据库实现接口监控。

##### 前提条件

需要下载并解压本地客户端安装包或者执行引擎包，具体可参见[《SoFlu社区版快速入门教程：1.下载本地客户端》](https://gitee.com/feisuanyz/SoFlu-adp/blob/master/SoFlu%E7%A4%BE%E5%8C%BA%E7%89%88%E6%95%99%E7%A8%8B/SoFlu%E7%A4%BE%E5%8C%BA%E7%89%88%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8%E6%95%99%E7%A8%8B/SoFlu%E7%A4%BE%E5%8C%BA%E7%89%88%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8%E6%95%99%E7%A8%8B.md#1-%E4%B8%8B%E8%BD%BD%E6%9C%AC%E5%9C%B0%E5%AE%A2%E6%88%B7%E7%AB%AF) 或[《第8章 项目管理：1.15 项目部署》](https://gitee.com/feisuanyz/SoFlu-adp/blob/master/SoFlu%E7%A4%BE%E5%8C%BA%E7%89%88%E6%95%99%E7%A8%8B/SoFlu%E7%A4%BE%E5%8C%BA%E7%89%88%E5%9F%BA%E7%A1%80%E6%93%8D%E4%BD%9C%E6%8C%87%E5%8D%97/8.%20%E9%A1%B9%E7%9B%AE%E7%AE%A1%E7%90%86/%E9%A1%B9%E7%9B%AE%E7%AE%A1%E7%90%86.md#115-%E9%A1%B9%E7%9B%AE%E9%83%A8%E7%BD%B2)。

##### 3.3.1 操作步骤

a）在本地客户端安装包或执行引擎包路径下配置` application.yml `文件（路径下若未该文件，则需新建一个该文件），打开接口监控开关同时配置数据库信息，` application.yml `文件的具体配置如下：


```
# 健康检查开关配置
management:
    server:
    	 # 默认的监控信息查看端口配置
    	 port:9091
    metrics:
        # 监控信息同步导出到influxdb中的配置
        export:
            influx:
            	  # 是否同步开关（值为true时即可开启同步）
                enabled: true
                # influxdb同步地址
                uri: http://ip:port
                # influxdb同步库名
                db: flowcore_influxdb_dev
                # influxdb用户名
                user-name: admin
                # influxdb密码
                password: admin

# 监控信息上报开关(控制执行引擎中的所有项目是否上报监控信息的总开关)
metrics-report:
    enable: true
```

b）启动本地客户端或者本地执行引擎项目，如图3-6所示：

![3-6](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_1.png)

##### 图3-6 启动项目

c）打开接口的监控开关及字段采集数据，如图3-7、图3-8所示：

![3-7](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_2.png)

##### 图3-7 打开监控

![3-8](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_3.png)

##### 图3-8 采集数据

d）运行接口即可开启接口监控数据采集。

e）通过工具（如示例工具` xshell `）连接上配置文件中的` infludb `数据库，查看数据库中监控到的接口信息，如图3-9所示：

![3-9](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_4.png)

##### 图3-9 查看接口信息

f）输入命令` influx `连接，可以看到当前数据口连接的版本和地址，如图3-10所示：

![3-10](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_5.png)

##### 图3-10 查看版本和地址

g）输入命令` auth `，登录数据库的账户密码（该账号密码需与application.yml配置文件中的数据库连接信息保持一致），如图3-11所示：

![3-11](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_6.png)

##### 图3-11 登陆数据库

h）输入命令` show databases `，显示所有数据库信息，如图3-12所示：

![3-12](https://www.feisuanyz.com/fsimage/zc-image/jkgl/influxdb_7.png)

##### 图3-12 显示数据库信息