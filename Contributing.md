## Gitee贡献指南

参与 Gitee 中的仓库开发，最常用和推荐的首选方式是“ Fork + Pull Request ”模式。在“ Fork + Pull Request ”模式下，仓库参与者不必向仓库创建者申请提交权限，而是在自己的托管空间下建立仓库的派生（Fork）。至于在派生仓库中创建的提交，可以非常方便地利用 Gitee 的 Pull Request 工具向原始仓库的维护者发送 Pull Request。

#### 1. 什么是 Pull Request
Pull Request 是两个仓库提交变更的一种方式，通常用于 Fork 仓库与被 Fork 仓库的差异提交，同时也是一种非常好的团队协作方式。

Gitee 平台限制 Pull Request 源仓库与目标仓库需存在 Fork 与被 Fork 关系，故如果您要提交 Pull Request，必须先 Fork 一个仓库，然后才能对该仓库提交 Pull Request，同时，以该仓库为父仓库的所有仓库，您也均可以提交 Pull Request。

#### 2. 具体教程

##### a）对目标仓库进行 Fork 操作
Fork 仓库是非常简单的，进到仓库页面，找到右上角的 Fork 按钮，点击后选择 Fork 到的命名空间，再点击确认，等待系统在后台完成仓库克隆操作，就完成 Fork 操作，如图 1-1 所示：

![Fork到命名空间](https://images.gitee.com/uploads/images/2021/0831/093644_010a73b4_8721401.png "2-1.png")
###### 图 1-1 Fork 到命名空间

##### b）将目标仓库克隆到本地：

```
git clone https://gitee.com/feisuanyz/adp-component.git
```
##### c）在本地从 master 中拉取一个分支 dev ：

```
git branch dev
git checkout dev
```
##### d）把您修改的内容提交到 dev 分支：

```
git commit -m "优化XXX"  //提交并备注提交信息
```
##### e）新增一个 push 地址，这个地址是您 Fork 项目后，被 Fork 项目的空间：

```
git remote add myDev https://gitee.com/test/adp-component.git
```
##### f）推送本地的分支 dev 到被 Fork 项目的空间：

```
git fetch origin      //获取本地的原来的 master
git merge orighin/dev     //将 dev 合并到 master
git push myDev dev      //将本地的最新的分支 push 到被 Fork 项目的空间
```
##### g）点击被 Fork 项目的仓库上方的 Pull Request，填入 Pull Request 的说明，点击提交 Pull Request，就可以了。

#### 3. 关于提交 Pull Request 的问题
首先，您的仓库与目标仓库必须存在差异，这样才能提交，如图 2-1 所示：

![提交PR](https://images.gitee.com/uploads/images/2021/0831/094955_75a2c0b0_8721401.png "2-1.png")
###### 图 2-1 提交 Pull Request

如果不存在差异，或者目标分支比您提 Pull Request 的分支还要新，则会得到图 2-2 所示提示：

![提交错误](https://images.gitee.com/uploads/images/2021/0831/095111_a6dee610_8721401.png "2-2.png")
###### 图 2-2 提交错误

然后，填入 Pull Request 的说明，点击提交 Pull Request，就可以提交一个 Pull Request 了，如图 2-3 所示：

![成功提交PR](https://images.gitee.com/uploads/images/2021/0831/095204_f8f3a4ab_8721401.png "2-3.png")
###### 图 2-3 成功提交 Pull Request

#### 4. 如何对已经存在的 Pull Request 的进行管理

首先，对于一个已经存在的 Pull Request，如果只是观察者，报告者等权限，那么访问将会受到限制，具体权限限制请参考 [Gitee 平台关于角色权限的内容](https://gitee.com/help/articles/4288)，下文涉及的部分，仅针对管理员权限，如果您发现不太一样的地方，请检查您的权限是不是管理员或该 Pull Request 的创建者。

#### 5. 如何修改一个已经存在的 Pull Request

点击 Pull Request 的详情界面右上角的编辑按钮，就会弹出编辑框，在编辑框中修改您需要修改的信息，然后点击保存即可修改该 Pull Request，如图 3-1 所示：

![修改PR](https://images.gitee.com/uploads/images/2021/0831/095618_05277ea7_8721401.png "3-1.png")
###### 图 3-1 修改 Pull Request

请注意，在该界面，可以对 Pull Request 进行指派负责人，指派测试者等等操作，每一个操作均会通知对应的人员。

#### 6. 对 Pull Request 的 bug 修改如何提交到该 Pull Request 中

对于 Pull Request 中的 bug 修复或者任何更新动作，均不必要提交新的 Pull Request，仅仅只需要推送到您提交 Pull Request 的分支上，稍后我们后台会自动更新这些提交，将其加入到这个 Pull Request 中去。

#### 7. Pull Request 不能自动合并该如何处理

在提交完 Pull Request 后，在这个 Pull Request 处理期间，由原本的能自动合并变成不能自动合并，这是一件非常正常的事情，那么，这时，我们有两种选择，一种，继续合并到目标，然后手动处理冲突部分，另一种则是先处理冲突，使得该 Pull Request 处于可以自动合并状态，然后采用自动合并，一般来讲，我们官方推荐第二种，即先处理冲突，然后再合并。具体操作为：

先在本地切换到提交 Pull Request 的分支，然后拉取目标分支到本地，这时，会发生冲突，参考[如何处理代码冲突](https://gitee.com/help/articles/4194)这一小节内容将冲突处理完毕，然后提交到 Pull Request 所在的分支，等待系统后台完成 Pull Request 的更新后，Pull Request 就变成了可自动合并状态。

#### 8. Pull Request 不小心合并了，可否回退

对于错误合并的 Pull Request，我们提供了回退功能，该功能会产生一个回退 XXX 的 Pull Request，接受该 Pull Request 即可完成回退动作，注意，回退本质上是提交一个完全相反的 Pull Request，所以，您仍然需要进行测试来保证完整性。此外，为了不破坏其他 Pull Request，建议只有需回退的 Pull Request 处于最后一次合并操作且往上再无提交时执行回退动作，否则请手动处理。

#### 9. 其他疑问
如有进一步疑问，可查询 Gitee 官方帮助中心：https://gitee.com/help 或通过[ SoFlu 开发者社区](https://gitee.com/feisuanyz/developer-community)各个渠道反馈。

#### 10. 版权声明

本贡献指南基于以下内容进行修改：

1）Gitee 的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。

原文链接：https://gitee.com/help/articles/4128#article-header0

2）CSDN 博主「luck-cheng」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。

原文链接：https://blog.csdn.net/qq_20032995/article/details/84502141
